using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Entities;

using Ecs.Systems.InventorySystem;


public class PlayerInput : MonoBehaviour
{
    private Vector2Int[] ScreenSizes = { new Vector2Int(1280, 720), new Vector2Int(1920, 1080), new Vector2Int(3860, 2160) };
    private int ScreenSizeIndex = 1;
    private InventorySystem InventorySystem;

    private void Awake()
    {
        InventorySystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<InventorySystem>();
        InventorySystem.ScreenSize = ScreenSizes[ScreenSizeIndex];
    }

    public void CycleResolutionDown(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        ScreenSizeIndex--;
        if (ScreenSizeIndex < 0)
            ScreenSizeIndex = ScreenSizes.Length - 1;

        InventorySystem.ScreenSize = ScreenSizes[ScreenSizeIndex];
        InventorySystem.ResizeInventory();
    }

    public void CycleResolutionUp(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        ScreenSizeIndex++;
        if (ScreenSizeIndex >= ScreenSizes.Length)
            ScreenSizeIndex = 0;

        InventorySystem.ScreenSize = ScreenSizes[ScreenSizeIndex];
        InventorySystem.ResizeInventory();
    }

    public void MoveLeft(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        InventorySystem.MoveSelector(-1, 0);
    }

    public void MoveRight(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        InventorySystem.MoveSelector(1, 0);
    }

    public void MoveUp(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        InventorySystem.MoveSelector(0, -1);
    }

    public void MoveDown(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        InventorySystem.MoveSelector(0, 1);
    }

    public void Delete(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        if(InventorySystem.SelectorItemPickedUp)
        {
            // delete item
            InventorySystem.DeleteItem();
        }
        else
        {
            // reset items
            InventorySystem.ClearItems();
            InventorySystem.CreateItems();
        }
    }

    public void Pickup(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started)
            return;

        if(!InventorySystem.SelectorItemPickedUp)
        {
            // attempt to pick up item
            InventorySystem.PickupItem();
        }
        else
        {
            // place item
            InventorySystem.DropItem();
        }
    }
}
