using UnityEngine;
using Unity.Entities;
using Ecs.Systems.InventorySystem;


public class ItemDescriptionBox : MonoBehaviour
{
    public GameObject DescriptionBox => descriptionBox;
    [SerializeField] private GameObject descriptionBox;

    void Awake()
    {
        // register inventory to system
        var InventorySystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<InventorySystem>();
        InventorySystem.ItemDescriptionBox = descriptionBox;
    }
}
