using UnityEngine;
using UnityEngine.UI;

using Unity.Entities;
using Ecs.Systems.InventorySystem;
using Ecs.Spawners.InventoryEntitySpawner;
using System.Collections.Generic;

public class MenuInventory : MonoBehaviour
{
    // this class registers the Inventory GameObject to the Inventory System

    public InventorySystem InventorySystem { get; private set; }

    public GameObject ItemSlotPrefab => itemSlotPrefab;
    public GameObject InventoryGameObject => inventoryGameObject;
    public GameObject SelectorPrefab => selectorPrefab;
    public int InventorySlotCount => inventorySlotCount;
    public int InventorySlotWidth => inventorySlotWidth;
    public int SlotPxSize => slotPxSize;
    public Vector2Int SlotOffset => slotOffset;
    public Vector2Int SelectorOffset => selectorOffset;


    [Space] [Header("Item Slots")]
    [SerializeField] private GameObject inventoryGameObject;
    [SerializeField] private int inventorySlotCount = 18;
    [SerializeField] private int inventorySlotWidth = 6;
    
    [Space] [Header("Item Slots")]
    [SerializeField] private GameObject itemSlotPrefab;
    [SerializeField] private Vector2Int slotOffset = new Vector2Int(9, 8);
    [SerializeField] private int slotPxSize = 19;

    [Space] [Header("Selector")]
    [SerializeField] private GameObject selectorPrefab;
    [SerializeField] private Vector2Int selectorOffset = new Vector2Int( 6, -5 );
    public AudioClip[] SoundMove = new AudioClip[3];
    public AudioClip SoundMoveEmpty;
    public AudioClip SoundPickup;
    public AudioClip SoundDrop;
    public AudioClip SoundDelete;

    void Start()
    {
        #region register inventory to system

        InventorySystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<InventorySystem>();
        InventorySystem.Inventory = inventoryGameObject;

        RectTransform invTransform = (RectTransform)inventoryGameObject.transform;
        Vector3 scale = invTransform.localScale;

        #endregion

        #region set system inventory size

        InventorySystem.InventorySize = inventorySlotCount;
        InventorySystem.InventorySlot = new GameObject[inventorySlotCount];

        #endregion

        #region create inventory slots
        for ( int i = 0; i < inventorySlotCount; i++ )
        {
            GameObject slot = Instantiate(ItemSlotPrefab, new Vector3(0, 0, 0), Quaternion.identity, inventoryGameObject.transform);

            // set position
            RectTransform slotTransform = (RectTransform)slot.transform;

            float ix = scale.x * (slotOffset.x + slotPxSize * (i % inventorySlotWidth));
            float iy = scale.y * (slotOffset.y + slotPxSize * 2 - slotPxSize * Mathf.Floor(i / inventorySlotWidth));

            slotTransform.Translate(new Vector3(ix, iy, 0));

            // register slot to inventory system
            InventorySystem.InventorySlot[i] = slot;
        }
        #endregion

        #region  create inventory selector
        GameObject selector = Instantiate(selectorPrefab, new Vector3(0, 0, 0), Quaternion.identity, inventoryGameObject.transform);
        RectTransform selectorTransform = (RectTransform)selector.transform;

        selectorTransform.Translate(new Vector3(selectorOffset.x * scale.x, (invTransform.sizeDelta.y + selectorOffset.y) * scale.y, 0));
        selectorTransform.SetAsLastSibling();

        InventorySystem.Selector = selector;
        InventorySystem.SelectorAudioSource = selector.GetComponent<AudioSource>();

        InventorySystem.MoveSounds = SoundMove;
        InventorySystem.PickupSound = SoundPickup;
        InventorySystem.DropSound = SoundDrop;
        InventorySystem.DeleteSound = SoundDelete;
        InventorySystem.MoveEmptySound = SoundMoveEmpty;

        #endregion

        // resize inventory
        InventorySystem.ResizeInventory();

        // spawn 5 staring items 
        InventorySystem.CreateItems();
    }
}
