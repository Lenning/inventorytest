using UnityEngine;
using Unity.Entities;
using Ecs.Systems.InventorySystem;

public class ItemText : MonoBehaviour
{
    public GameObject ItemTextObject => itemText;
    [SerializeField] private GameObject itemText;

    void Awake()
    {
        // register inventory to system
        var InventorySystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<InventorySystem>();
        InventorySystem.ItemText = itemText;
    }
}
