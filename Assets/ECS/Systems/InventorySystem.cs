using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using TMPro;

using System;
using System.Collections.Generic;

using Ecs.Components.InventoryComponents;
using Ecs.Spawners.InventoryEntitySpawner;

// This system manages the interface visuals

namespace Ecs.Systems.InventorySystem
{
    [AlwaysUpdateSystem]
    public class InventorySystem : SystemBase
    {
        public static GameObject Inventory;
        public static GameObject Selector;
        public static GameObject ItemDescriptionBox;
        public static GameObject ItemText;
        public static GameObject[] InventorySlot;

        public static AudioSource SelectorAudioSource;
        public static AudioClip[] MoveSounds;
        public static AudioClip MoveEmptySound;
        public static AudioClip PickupSound;
        public static AudioClip DropSound;
        public static AudioClip DeleteSound;

        public static int InventorySize = 18;
        public static bool SlotsNeedUpdate = true;
        public static bool SelectorItemPickedUp = false;
        public static Vector2Int SelectorOffset = new Vector2Int(6, -5);
        public static int SlotPxSize = 19;

        public static Vector2Int ScreenSize {
            get {
#if UNITY_EDITOR

                return new Vector2Int(Screen.width, Screen.height);
#else
                return screenSize;
#endif
            }
            set {
                int sWidth = Mathf.Clamp(value.x, 0, Screen.currentResolution.width);
                int sHeight = Mathf.Clamp(value.y, 0, Screen.currentResolution.height);

                if(sWidth/16 > sHeight/9)
                    sWidth = (sHeight / 9)*16;
                else if(sWidth / 16 < sHeight / 9)
                    sHeight = (sWidth / 16) * 9;

                screenSize = new Vector2Int(sWidth,sHeight);
                Screen.SetResolution(sWidth, sHeight, false);
            }
        }
        private static Vector2Int screenSize = new Vector2Int(1920, 1080);

        private float inventoryScaleTarget = 0.55f;
        private Vector2Int selectorPosition = new Vector2Int(0, 0);
        private bool selectorUpdated = true;
        private bool displayBoxUpdate = false;
        private readonly float descriptionTimerDelay = 0.5f;
        private float descriptionTimer = 0.5f;
        private int descriptionOffset = 0;
        private string displayText = "";
        private Entity PickUp;
        private Entity Swap;
        private int pickupIndex;
        private int swapIndex;

        public void ResizeInventory()
        {
            ResetDisplayOffset();

            RectTransform transform = (RectTransform)Inventory.transform;

            // set inventory scale
            float targetWidth = ScreenSize.x * inventoryScaleTarget;
            float targetHeigth = ScreenSize.y * inventoryScaleTarget;

            float scale = Mathf.Floor(Mathf.Min(targetWidth / transform.sizeDelta.x, targetHeigth / transform.sizeDelta.y));
            transform.localScale = new Vector3(scale, scale, 1);

            // set inventory position
            int newX = (int)Mathf.Round(scale * (ScreenSize.x * 0.5f / scale - transform.sizeDelta.x * 0.5f));
            //int newY = (int)Mathf.Round(scale * (ScreenSize.y * 0.5f / scale - transform.sizeDelta.y * 0.75f));
            //int newX = (int)scale * 4;
            int newY = (int)Mathf.Round(ScreenSize.y - (transform.sizeDelta.y + 36) * scale);

            transform.anchoredPosition = new Vector2(newX, newY);

            //((RectTransform)Selector.transform).sizeDelta = new Vector2(50,50);

            // update display box
            displayBoxUpdate = true;
        }

        public void MoveSelector(int x, int y)
        {
            // move selector
            selectorPosition.x += x;
            if (selectorPosition.x < 0)
                selectorPosition.x = 5;
            else if (selectorPosition.x >= 6)
                selectorPosition.x = 0;

            selectorPosition.y += y;
            if (selectorPosition.y < 0)
                selectorPosition.y = 2;
            else if (selectorPosition.y >= 3)
                selectorPosition.y = 0;

            // update items
            if (SelectorItemPickedUp)
            {
                UpdatePickedUpItems();
                SlotsNeedUpdate = true;

                // play sound
                SelectorAudioSource.PlayOneShot(MoveSounds[UnityEngine.Random.Range(0, MoveSounds.Length)]);
            }
            else
            {
                // play sound
                SelectorAudioSource.PlayOneShot(MoveEmptySound);
            }

            // update selector
            descriptionTimer = descriptionTimerDelay;
            UpdateSelectorPosition();
        }

        public void CreateItems()
        {
            // spawn 5 staring items 
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            List<int> slotIndexes = new List<int>();
            int index = UnityEngine.Random.Range(0, InventorySize);

            for (int i = 0; i < 5; i++)
            {
                while (slotIndexes.Contains(index))
                    index = UnityEngine.Random.Range(0, InventorySize);

                slotIndexes.Add(index);
                Spawner.CreateInventoryItem(ref entityManager, index, UnityEngine.Random.Range(0, 52));
            }

            SlotsNeedUpdate = true;
            selectorUpdated = true;
            descriptionTimer = descriptionTimerDelay;
        }

        public void ClearItems()
        {
            // clear all items
            EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob, PlaybackPolicy.MultiPlayback);

            Entities.WithoutBurst().WithAll<Item>().ForEach((Entity item) =>
            {
                ecb.DestroyEntity(item);
            }).Run();

            ecb.Playback(World.DefaultGameObjectInjectionWorld.EntityManager);
            ecb.Dispose();

            ClearDisplayText();

            SelectorItemPickedUp = false;
            PickUp = Entity.Null;
            Swap = Entity.Null;
            descriptionTimer = descriptionTimerDelay;

            SelectorAudioSource.PlayOneShot(DeleteSound);
        }

        public void PickupItem()
        {
            int selectorIndex = selectorPosition.x + selectorPosition.y * 6;

            Entities.WithoutBurst().WithAll<Item, ItemSlotIndex, ItemName>().ForEach((Entity item, ref ItemName itemName, in ItemSlotIndex slotIndex) =>
            {
                if (slotIndex.value == selectorIndex)
                {
                    PickUp = item;
                }
            }).Run();

            if (PickUp != Entity.Null)
            {
                SelectorItemPickedUp = true;
                pickupIndex = selectorIndex;

                UpdateSelectorPosition();

                // play sound
                SelectorAudioSource.PlayOneShot(PickupSound);
            }
            else
            {
                SelectorItemPickedUp = false;
            }
        }

        public void DropItem()
        {
            SelectorItemPickedUp = false;
            PickUp = Entity.Null;
            Swap = Entity.Null;
            UpdateSelectorPosition();
            SlotsNeedUpdate = true;
            descriptionTimer = descriptionTimerDelay;

            // play sound
            SelectorAudioSource.PlayOneShot(DropSound);
        }

        public void DeleteItem()
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            entityManager.DestroyEntity(PickUp);
            PickUp = Entity.Null;
            SelectorItemPickedUp = false;

            if (Swap != Entity.Null)
            {
                entityManager.SetComponentData<ItemSlotIndex>(Swap, new ItemSlotIndex { value = swapIndex });
                Swap = Entity.Null;
            }

            SlotsNeedUpdate = true;
            UpdateSelectorPosition();
            descriptionTimer = descriptionTimerDelay;

            SelectorAudioSource.PlayOneShot(DeleteSound);
        }

        private void UpdateSlots()
        {
            // set all slots to blank state
            for (int i = 0; i < InventorySize; i++)
            {
                var image = InventorySlot[i].GetComponent<Image>();
                image.enabled = false;
            }

            // get the entity manager
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            bool itemSwapped = (Swap != Entity.Null);

            // set item data
            Entities.WithoutBurst().WithAll<Item, ItemSlotIndex, ItemSpriteIndex>().ForEach((Entity item, in ItemSlotIndex slotIndex, in ItemSpriteIndex spriteIndex) =>
            {
                var slot = InventorySlot[slotIndex.value];

                var image = slot.GetComponent<Image>();
                image.enabled = true;

                var material = new Material(image.material);

                // set item sprite
                material.SetVector("SpriteCoordinates", new Vector4(entityManager.GetComponentData<ItemSpriteIndex>(item).value, 0, 0, 0));

                // set item background
                if (entityManager.HasComponent<ItemBackgroundIndex>(item))
                {
                    material.SetFloat("EnableSpriteBackground", 1.0f);
                    material.SetVector("BackgroundCoordinates", new Vector4(entityManager.GetComponentData<ItemBackgroundIndex>(item).value, 0, 0, 0));
                }
                else
                    material.SetFloat("EnableSpriteBackground", 0.0f);

                // set swapped item alpha
                if (itemSwapped && slotIndex.value == pickupIndex)
                    material.SetFloat("Alpha", 0.5f);
                else
                    material.SetFloat("Alpha", 1f);

                image.material = material;
            }).Run();
        }

        private void UpdatePickedUpItems()
        {
            int selectorIndex = selectorPosition.x + selectorPosition.y * 6;
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            // swap back swapped item
            if(Swap != Entity.Null)
            {
                entityManager.SetComponentData<ItemSlotIndex>(Swap, new ItemSlotIndex { value = swapIndex });
                Swap = Entity.Null;
            }

            // look for other items on the new selector position
            Entities.WithoutBurst().WithAll<Item, ItemSlotIndex, ItemName>().ForEach((Entity item, ref ItemName itemName, in ItemSlotIndex slotIndex) =>
            {
                if (slotIndex.value == selectorIndex)
                {
                    Swap = item;
                }
            }).Run();

            // set picked up item new place
            entityManager.SetComponentData<ItemSlotIndex>(PickUp, new ItemSlotIndex { value = selectorIndex });

            // set swaped item place
            if (Swap != Entity.Null)
            {
                entityManager.SetComponentData<ItemSlotIndex>(Swap, new ItemSlotIndex { value = pickupIndex });
                swapIndex = selectorIndex;
            }
        }

        private void UpdateSelectorPosition()
        {

            RectTransform transform = (RectTransform)Selector.transform;

            int newX = selectorPosition.x * SlotPxSize + SelectorOffset.x;
            int newY = -selectorPosition.y * SlotPxSize + SelectorOffset.y;

            transform.anchoredPosition = new Vector2(newX, newY);

            selectorUpdated = true;

            var textMesh = ItemText.GetComponent<TextMeshProUGUI>();
            textMesh.text = "";
            displayText = "";

            var image = ItemDescriptionBox.GetComponent<Image>();
            image.enabled = false;
        }

        private void ClearDisplayText()
        {
            displayText = "";

            var textMesh = ItemText.GetComponent<TextMeshProUGUI>();
            textMesh.text = displayText;

            var image = ItemDescriptionBox.GetComponent<Image>();
            image.enabled = false;

            SlotsNeedUpdate = true;
            selectorUpdated = true;
        }

        private void UpdateDisplayText()
        {
            var textMesh = ItemText.GetComponent<TextMeshProUGUI>();

            if (!SelectorItemPickedUp && descriptionTimer <= 0)
            {
                textMesh.text = displayText;
            }
            else
            {
                textMesh.text = "";
                displayText = "";
            }

            displayBoxUpdate = true;
        }

        private void UpdateDisplayTextBox()
        {
            var textMesh = ItemText.GetComponent<TextMeshProUGUI>();
            var image = ItemDescriptionBox.GetComponent<Image>();
            ResetDisplayOffset();

            if (SelectorItemPickedUp || descriptionTimer > 0)
            {
                image.enabled = false;
            }
            else
            {
                image.enabled = true;

                RectTransform transform = (RectTransform)ItemDescriptionBox.transform;
                transform.sizeDelta = new Vector2(Mathf.Round(textMesh.renderedWidth) + 5, Mathf.Round(textMesh.renderedHeight) + 5);

                PreformDisplayOffset();
            }
        }

        private void ResetDisplayOffset()
        {
            RectTransform transform = (RectTransform)ItemDescriptionBox.transform;
            transform.anchoredPosition = new Vector2(transform.anchoredPosition.x - descriptionOffset, transform.anchoredPosition.y);
            descriptionOffset = 0;
        }

        private void PreformDisplayOffset()
        {
            if (descriptionOffset != 0)
            {
                ResetDisplayOffset();
            }

            RectTransform displayTransform = (RectTransform)ItemDescriptionBox.transform;
            RectTransform selectorTransform = (RectTransform)Selector.transform;
            RectTransform inventoryTransform = (RectTransform)Inventory.transform;
            
            float scale = inventoryTransform.localScale.x;
            float maxPositionX = inventoryTransform.anchoredPosition.x + (selectorTransform.anchoredPosition.x + selectorTransform.sizeDelta.x + displayTransform.anchoredPosition.x + displayTransform.sizeDelta.x) * scale;

            if (maxPositionX > ScreenSize.x)
            {
                descriptionOffset = Mathf.Clamp( (int)Mathf.Floor( (ScreenSize.x - maxPositionX) / scale), (int)((-(maxPositionX-displayTransform.sizeDelta.x*scale)) / scale), 0);
                displayTransform.anchoredPosition = new Vector2(displayTransform.anchoredPosition.x + descriptionOffset, displayTransform.anchoredPosition.y);
            }
        }


        protected override void OnUpdate()
        {
            if (Inventory == null)
                return;

            // update timer
            if (descriptionTimer > 0)
            {
                descriptionTimer -= Time.DeltaTime;

                if (descriptionTimer <= 0)
                {
                    selectorUpdated = true;
                }
            }

            // update inventory slots
            if (SlotsNeedUpdate)
            {
                UpdateSlots();
                SlotsNeedUpdate = false;
            }

            // update display box
            if(displayBoxUpdate)
            {
                UpdateDisplayTextBox();
                displayBoxUpdate = false;
            }

            // update selector
            if(selectorUpdated)
            {
                var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
                int selectorIndex = selectorPosition.x + selectorPosition.y * 6;

                Entities.WithoutBurst().WithAll<Item, ItemSlotIndex, ItemName>().ForEach((Entity item, ref ItemName itemName, in ItemSlotIndex slotIndex) =>
                {
                    if (slotIndex.value == selectorIndex) 
                    {
                        displayText = itemName.name.ConvertToString();
                        if (entityManager.HasComponent<ItemBackgroundIndex>(item))
                        {

                        }
                    }
                }).Run();

                // set display text
                if( displayText != "" )
                {
                    UpdateDisplayText();
                }

                selectorUpdated = false;
            }
        }
    }
}
