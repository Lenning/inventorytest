using Unity.Entities;
using Unity.Collections;

namespace Ecs.Components.InventoryComponents
{
    public struct Item : IComponentData { } // tag

    public struct ItemSlotIndex : IComponentData
    {
        public int value;
    }

    public struct ItemSpriteIndex : IComponentData
    {
        public int value;
    }

    public struct ItemBackgroundIndex : IComponentData
    {
        public int value;
    }

    public struct ItemName : IComponentData
    {
        public FixedString128 name;
    }

    public struct ItemQuality : IComponentData
    {
        public int value;
    }

    public struct ItemDescription : IComponentData
    {
        public FixedString4096 text;
    }
    
}