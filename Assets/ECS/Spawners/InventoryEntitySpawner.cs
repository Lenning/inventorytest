using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Ecs.Components.InventoryComponents;

namespace Ecs.Spawners.InventoryEntitySpawner
{
    public static class Spawner
    {
        public static string GetInventoryItemName(int index)
        {
            switch (index)
            {
                case 0: return "Short Sword";
                case 1: return "Wooden Bow";
                case 2: return "Spear";
                case 3: return "Shovel";
                case 4: return "Broadsword";
                case 5: return "Woodcutter's Axe";
                case 6: return "Pickaxe";
                case 7: return "Falchion";
                case 8: return "Arrow";
                case 9: return "Poisionous Arrow";
                case 10: return "Robe";
                case 11: return "Leather Armor";
                case 12: return "Steel Cuirass";
                case 13: return "Wooden Shield";
                case 14: return "Leather Boots";
                case 15: return "Leather Helmet";
                case 16: return "Staff";
                case 17: return "Wand";
                case 18: return "Amber Ring";
                case 19: return "Sapphire Pendant";
                case 20: return "Health Potion";
                case 21: return "Large Health Potion";
                case 22: return "Mana Potion";
                case 23: return "Poision";
                case 24: return "42 Gold";
                case 25: return "Ruby";
                case 26: return "Small Locked Chest";
                case 27: return "Key";
                case 28: return "Steak";
                case 29: return "Apple";
                case 30: return "Carrot";
                case 31: return "Sunflower";
                case 32: return "Grilled Fish";
                case 33: return "Storage Device";
                case 34: return "Iron Gear";
                case 35: return "Dotted Balloon";
                case 36: return "Hearth";
                case 37: return "Water";
                case 38: return "Monster Teeth";
                case 39: return "Frozen Fireball";
                case 40: return "Vorpal Sword";
                case 41: return "Fiery Sword";
                case 42: return "Battle Axe";
                case 43: return "Enchanted Shield";
                case 44: return "Weeping Blood Helmet";
                case 45: return "The Eye";
                case 46: return "Health Kit";
                case 47: return "Bloodletter";
                case 48: return "Electrified Shield";
                case 49: return "Shroom";
                case 50: return "Eye Worm";
                case 51: return "Green Ooze Blob";

                default: return "";
            }
        }

        public static string GetInventoryItemDescription(int index)
        {
            switch (index)
            {
                case 0: return "Simple yet usefull.";
                case 1: return "Arrows not included.";
                case 2: return "For stabbing at a distance.";
                case 3: return "Remember to say you're an archaeologist.";
                case 4: return "The extra thickness makes all the difference.";
                case 5: return "For all your woodcutting needs.";
                case 6: return "Never dig straight down.";
                case 7: return "Fancy!";
                case 8: return "Bow not included.";
                case 9: return "DC 12 Constitution save.";
                case 10: return "Better than nothing.";
                case 11: return "Sturdy armor for when you need to be quick on your feet.";
                case 12: return "Expensive but worth it.";
                case 13: return "Warning: Does not reflect dragonfire.";
                case 14: return "Well worn travel companions.";
                case 15: return "Protects your noggin.";
                case 16: return "Doubles as walking stick.";
                case 17: return "Allows you to do wizzardy things.";
                case 18: return "You feel more fabiolous wearing this.";
                case 19: return "+9 mana regeneration.";
                case 20: return "Never use, save for urgent situations.";
                case 21: return "For extra large needs.";
                case 22: return "For when your wizzard is feeling thirsty";
                case 23: return "A coward's weapon.";
                case 24: return "Never too much.";
                case 25: return "Red gemstone, moderately valuable.";
                case 26: return "Now where's the key for this thing again?";
                case 27: return "For opening, something...";
                case 28: return "Better cook this first.";
                case 29: return "Yum!";
                case 30: return "Eh, what other kinds of food have you?";
                case 31: return "Pretty.";
                case 32: return "Roasted over a wooden fire, could use a dash of lemon.";
                case 33: return "Stores up to 16 scrolls in such a small size.";
                case 34: return "Dwarven made, very sturdy.";
                case 35: return "Exclames: I'm just about to say something.";
                case 36: return "For a model, it's not that acurate.";
                case 37: return "Splashy!";
                case 38: return "Or where they stalagmites?";
                case 39: return "Frozen in time, warm to the touch.";
                case 40: return "Outch!";
                case 41: return "Main use: Hydras";
                case 42: return "For when you don't feel like talking.";
                case 43: return "Unfortunatly, the enchantment is useless.";
                case 44: return "Intimidating, but constantly gets blood in your eyes.";
                case 45: return "Sees only hidden things.";
                case 46: return "Works instantly, somehow.";
                case 47: return "Bloody ghost.";
                case 48: return "Their in for a surprise.";
                case 49: return "This was the non-poisonous kind right?";
                case 50: return "Main ingredient in Witches' brew";
                case 51: return "He's called bob.";

                default: return "";
            }
        }

        public static string GetInventoryItemQualityName(int quality)
        {
            switch (quality)
            {
                case 0: return " (Common)";
                case 1: return " (Rare)";
                case 2: return " (Epic)";

                default: return "";
            }
        }

        public static bool GetInventoryItemBackground(int index)
        {
            switch (index)
            {
                case 0: return true;
                case 1: return true;
                case 2: return true;
                case 4: return true;
                case 7: return true;
                case 11: return true;
                case 12: return true;
                case 13: return true;
                case 14: return true;
                case 15: return true;
                case 16: return true;
                case 17: return true;
                case 40: return true;
                case 41: return true;
                case 42: return true;
                case 43: return true;
                case 44: return true;
                case 47: return true;
                case 48: return true;

                default: return false;
            }
        }

        public static Entity CreateInventoryItem(ref EntityManager entityManager, int slotIndex, int spriteIndex)
        {
            EntityArchetype archetype = entityManager.CreateArchetype(
                typeof(Item),               // tag
                typeof(ItemSlotIndex),      // slot index
                typeof(ItemName),           // name
                //typeof(ItemDescription),    // description
                typeof(ItemSpriteIndex));   // sprite index

            Entity entity = entityManager.CreateEntity(archetype);

            string name = GetInventoryItemName(spriteIndex);
            //entityManager.SetName(entity, name);

            if (GetInventoryItemBackground(spriteIndex))
            {
                int quality = Random.Range(0,3);
                entityManager.AddComponentData(entity, new ItemQuality { value = quality });
                entityManager.AddComponentData(entity, new ItemBackgroundIndex { value = quality + 52 });
                name += GetInventoryItemQualityName(quality);
            }

            entityManager.SetComponentData(entity, new ItemSlotIndex() { value = slotIndex });
            entityManager.SetComponentData(entity, new ItemName() { name = new FixedString128(name) });
            //entityManager.SetComponentData(entity, new ItemDescription() { text = new FixedString4096(GetInventoryItemDescription(spriteIndex)) });
            entityManager.SetComponentData(entity, new ItemSpriteIndex() { value = spriteIndex });

            

            return entity;
        }
    }

}